gText_Birch_Welcome:: @ 82C897B
	.string "Hello!\p"
	.string "My name is Professor Gneiss.\n"
	.string "Welcome to the world of POKéMON.\p"
	.string "In case you didn’t know, POKéMON are\n"
	.string "creatures which reside with us on\l"
	.string "this very planet.\p"
	.string "I’d consider them man’s best friend.\p"
	.string "$"

gText_Birch_Pokemon:: @ 82C89FB
	.string "Here’s an example of one."
	.string "\n"
	.string "$"

gText_Birch_MainSpeech:: @ 82C8A1F
	.string "Each POKéMON is unique in their own\n"
	.string "way, but all POKéMON can act as\l"
	.string "playmates and workmates, and even\l"
	.string "hold their own in battles against\l"
	.string "one another.\p"
	.string "Even though they live with us, we\n"
	.string "don’t know everything about them.\l"
	.string "That’s what makes my job important.\p"

gText_Birch_AndYouAre:: @ 82C8BD0
	.string "But enough about me.\nWhat about you?$"

gText_Birch_BoyOrGirl:: @ 82C8BDD
	.string "Are you a boy?\n"
	.string "Or are you a girl?$"

gText_Birch_WhatsYourName:: @ 82C8BFF
	.string "What about your name?$"

gText_Birch_SoItsPlayer:: @ 82C8C1C
	.string "Your name is {PLAYER}{KUN}?$"

gText_Birch_YourePlayer:: @ 82C8C2A
	.string "Alrighty then!\p"
	.string "$"

gText_Birch_AreYouReady:: @ 82C8C7A
	.string "You are about to embark on a\n"
	.string "journey, which is both exciting and\l"
	.string "perilous.\p"
	.string "It is with this sentiment\n"
	.string "that I leave you to decide which\l"
	.string "path you will take.\p"
	.string "Now, off into the world of POKéMON!\p"
	.string "$"
