Route101_EventObjects::
	object_event 1, EVENT_OBJ_GFX_ITEM_BALL, 0, 26, 3, 3, MOVEMENT_TYPE_FACE_DOWN, 0, 0, FALSE, 0, NULL, 1000
	object_event 2, EVENT_OBJ_GFX_MAY_NORMAL, 0, 7, 10, 3, MOVEMENT_TYPE_FACE_RIGHT, 0, 0, FALSE, 0, NULL, FLAG_0x028
	object_event 3, EVENT_OBJ_GFX_BRENDAN_NORMAL, 0, 7, 10, 3, MOVEMENT_TYPE_FACE_RIGHT, 0, 0, FALSE, 0, NULL, FLAG_0x028

Route101_MapWarps::
	warp_def 32, 8, 3, 0, MAP_NEW_MAP2

Route101_MapCoordEvents::
	coord_event 3, 8, 3, VAR_0x40F9, 0, Route101_EventScript_1EBCD5
	coord_event 3, 9, 3, VAR_0x40F9, 0, Route101_EventScript_1EBCD5
	coord_event 3, 10, 3, VAR_0x40F9, 0, Route101_EventScript_1EBCD5
	coord_event 3, 11, 3, VAR_0x40F9, 0, Route101_EventScript_1EBCD5

Route101_MapBGEvents::
	bg_event 9, 13, 3, BG_EVENT_PLAYER_FACING_ANY, Route101Sign

Route101_MapEvents::
	map_events Route101_EventObjects, Route101_MapWarps, Route101_MapCoordEvents, Route101_MapBGEvents
