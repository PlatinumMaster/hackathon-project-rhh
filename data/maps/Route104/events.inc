Route104_EventObjects::
	object_event 1, EVENT_OBJ_GFX_BUG_CATCHER, 0, 12, 11, 3, MOVEMENT_TYPE_LOOK_AROUND, 0, 0, FALSE, 0, NULL, 0
	object_event 2, EVENT_OBJ_GFX_MAN_6, 0, 17, 28, 3, MOVEMENT_TYPE_FACE_DOWN, 0, 0, FALSE, 0, NULL, 0
	object_event 3, EVENT_OBJ_GFX_MAN_4, 0, 47, 25, 3, MOVEMENT_TYPE_FACE_DOWN, 0, 0, FALSE, 0, NULL, 0

Route104_MapWarps::
	warp_def 12, 6, 3, 0, MAP_ROUTE103
	warp_def 50, 34, 3, 1, MAP_NEW_MAP3
	warp_def 47, 24, 3, 0, MAP_ROUTE104

Route104_MapBGEvents::
	bg_event 48, 32, 3, BG_EVENT_PLAYER_FACING_ANY, NULL
	bg_event 13, 19, 3, BG_EVENT_PLAYER_FACING_ANY, NULL

Route104_MapEvents::
	map_events Route104_EventObjects, Route104_MapWarps, 0x0, Route104_MapBGEvents
