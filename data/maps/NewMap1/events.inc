NewMap1_EventObjects::
	object_event 1, EVENT_OBJ_GFX_SCIENTIST_1, 0, 1, 9, 3, MOVEMENT_TYPE_FACE_LEFT, 0, 0, FALSE, 0, NULL, 0
	object_event 2, EVENT_OBJ_GFX_PROF_BIRCH, 0, 4, 6, 3, MOVEMENT_TYPE_NONE, 0, 0, FALSE, 0, GniessScript, 0
	object_event 3, EVENT_OBJ_GFX_MAY_NORMAL, 0, 3, 7, 2, MOVEMENT_TYPE_FACE_UP, 0, 0, FALSE, 0, YouToo, FLAG_0x026
	object_event 4, EVENT_OBJ_GFX_BRENDAN_NORMAL, 0, 3, 7, 3, MOVEMENT_TYPE_FACE_UP, 0, 0, FALSE, 0, YouToo, FLAG_0x026

NewMap1_MapWarps::
	warp_def 3, 12, 3, 2, MAP_LITTLEROOT_TOWN_PROFESSOR_BIRCHS_LAB
	warp_def 4, 12, 3, 2, MAP_LITTLEROOT_TOWN_PROFESSOR_BIRCHS_LAB

NewMap1_MapEvents::
	map_events NewMap1_EventObjects, NewMap1_MapWarps, 0x0, 0x0
