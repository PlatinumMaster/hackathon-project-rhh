NewMap1_MapScripts::
	map_script 2, GniessScript_Pokemon
	.byte 0

GniessScript_Pokemon:
	map_script_2 VAR_0x40FD, 0, LevelScript
	.2byte 0
	
LevelScript:
	checkplayergender
	compare VAR_RESULT, 0 // If rival is female
	goto_eq FEMALE
	compare VAR_RESULT, 1 // If rival is male
	goto_eq MALE
	
FEMALE:
	removeobject 4
	setvar VAR_0x40FD, 1
	end
MALE:
	removeobject 3
	setvar VAR_0x40FD, 1
	end

@ Gniess Initial
GniessScript::
	compare VAR_0x40FE, 1
	goto_eq ThankYouForTheOffer
	lock
	faceplayer
	applymovement 3, Lookup
	applymovement 4, Lookup
	msgbox GniessText, 4
	closemessage
	delay 20
	goto GniessScript_YesNo
	end
	
GniessScript_YesNo:
	msgbox GniessYesNo, 5
	compare 0x800D, 0
	goto_eq GniessScript_WhyNo
	compare 0x800D, 1
	goto_eq GniessScript_Yes
	end
	
GniessScript_Yes:
	closemessage
	msgbox GniessYes_Text, 4
	closemessage
	delay 15
	msgbox GniessPokemonSelect_Text, 4
	closemessage
	delay 20
	applymovement 3, RivalMovement
	applymovement 4, RivalMovement
	msgbox GniessPokemonSelect_Text2, 4
	closemessage
	delay 20
	applymovement 255, PlayerMovement3
	waitmovement 0
	delay 20
	special ChooseStarter
	waitstate
	goto ChooseThisOne
	end
	
GniessScript_WhyNo:
	msgbox GniessWhyNo_Text, 4
	call GniessScript_YesNo
	end
	
ChooseThisOne::
	delay 20
	msgbox GoodChoice_Text, 4
	closemessage
	delay 20
	applymovement 3, RivalMovement2
	applymovement 4, RivalMovement2
	delay 20
	msgbox ILikeThisOne_Text
	closemessage
	delay 20
	applymovement 255, PlayerMovement2
	waitmovement 0
	delay 15
	applymovement 3, RivalMovement3
	applymovement 4, RivalMovement3
	delay 25
	msgbox WouldYouLikeToGiveNickNameText, 5
	compare VAR_RESULT, 1
	goto_eq WouldYouLikeToGiveNickName
	goto ChooseThisOne2
	end
	
ChooseThisOne2::
	msgbox Now_Text, 4
	playse 4
	delay 25
	msgbox Now_Text2, 4
	closemessage
	playfanfare MUS_FANFA4
	message LittlerootTown_ProfessorBirchsLab_Text_1FAC32
	waitfanfare
	setflag FLAG_SYS_POKEDEX_GET
	setflag FLAG_SYS_POKEMON_GET
	special sub_81AFDA0
	setflag FLAG_0x8E4
	msgbox Now_Text3, 4
	closemessage
	msgbox Now_Text4, 4
	closemessage
	closemessage
	applymovement 3, RivalMovement4
	waitmovement 0
	applymovement 4, RivalMovement4
	waitmovement 0
	removeobject 3
	removeobject 4
	setflag FLAG_0x026
	setvar VAR_0x40FC, 1
	setvar VAR_0x40FE, 1
	end
	
WouldYouLikeToGiveNickNameText:
	.string "While you’re at it, why not\n"
	.string "give the POKeMON a nickname?$"
	
WouldYouLikeToGiveNickName:
	fadescreen 1
	special ChangePokemonNickname
	waitstate
	goto ChooseThisOne2
	return
	
LittlerootTown_ProfessorBirchsLab_Text_1FAC32: @ 81FAC32
	.string "{PLAYER} received the POKéDEX!$"
	
GoodChoice_Text:
	.string "Good choice!$"
	
ILikeThisOne_Text:
	.string "ALEX: I really like this one!$"
	
Now_Text:
	.string "GNIESS: Good.\p"
	.string "Now that we have gotten that over\n"
	.string "with, I have one more thing to give\l"
	.string "you both.$"
Now_Text2:
	.string "This is a POKéDEX. We use these in\n"
	.string "order to capture data about each\l"
	.string "POKéMON we find in the wild.\p"
	.string "I will give one to both of you.$"
Now_Text3:
	.string "Your POKéDEX contains a special\n"
	.string "ability which allows you to\l"
	.string "resurrect fossils.\p"
	.string "However, the only way to resurrect\n"
	.string "fossils is if you have the three\l"
	.string "main parts of the POKéMON.\p"
	.string "This would be the head, the torso,\n"
	.string "and at least one of their other\l"
	.string "appendages.\p"
	.string "ALEX: But where are the fossils\n"
	.string "found?\p"
	.string "GNIESS: There are many excavation sites\n"
	.string "which can be found around the\l"
	.string "region.\p"
	.string "They are managed by me and my crew.\p"
	.string "Near some of these excavation sites,\n"
	.string "you can find Lab Checkpoints, where\l"
	.string "some of my aides reside.\p"
	.string "They are usually working in the\n"
	.string "excvation site, so if you need\l"
	.string "help, ask one of them.\p"
	.string "There happens to be an excavation\n"
	.string "site on Route 602, but no checkpoint\l"
	.string "since this lab is here.\p"
	.string "I advise that both of you go to\n"
	.string "this site to get some basic training.\p"
	.string "ALEX: Sounds like a plan!$"
Now_Text4:
	.string "See you there, {PLAYER}.$"

GniessWhyNo_Text:
	.string "Really? Have you given it a thought\n"
	.string "at all?\p" 
	.string "Let me ask you again.$"
	
GniessText:
	.string "Ah, you must be {PLAYER}.\p"
	.string "I’ve called you here today to\n"
	.string "propose a request.\p"
	.string "As you know, I’m a POKéMON\n"
	.string "Professor.\p"
	.string "But there’s something that separates\n"
	.string "me from most other professors.\p"
	.string "I actively do field research as an\n"
	.string "excavator. Why, you may ask?\p"
	.string "I noticed there are many POKéMON\n"
	.string "which no longer reside with us.\p"
	.string "But with recent innovations in\n"
	.string "technology, they can live once\l"
	.string "again.\p"
	.string "Obviously, I cannot do this alone.\p"
	.string "Which is why I called you both here\n"
	.string "today.$"
	
GniessYes_Text:
	.string "ALEX: I’d love to help you as well!\p"
	.string "GNIESS: Splendid! I’m glad I can\n"
	.string "count on you guys.\p"
	.string "First off, I’ll let you both choose\n"
	.string "POKéMON to be your partners.$"

GniessPokemonSelect_Text:
	.string "Here in this machine, there are 3\n"
	.string "POKéMON.\p"
	.string "Each of these POKéMON are unique in\n"
	.string "their own way, shape, and form.\p"
	.string "Which one of you will choose first?$"
	
GniessPokemonSelect_Text2:
	.string "ALEX: You can go first, {PLAYER}.$"
	
GniessYesNo:
	.string "Would you like to help me research\n"
	.string "these fossilized POKéMON?$"

	
Lookup:
	face_up
	step_end
	
RivalMovement:
	face_right
	step_end
	
PlayerMovement:
	walk_down
	face_up
	step_end
	
RivalMovement2:
	walk_right
	walk_right
	walk_right
	walk_right
	walk_up
	step_end
	
PlayerMovement2:
	walk_down
	walk_left
	walk_left
	face_up
	step_end
	
PlayerMovement3:
	walk_right
	walk_right
	walk_up
	step_end
	
RivalMovement3:
	walk_down
	walk_left
	walk_left
	walk_down
	walk_left
	walk_left
	walk_up
	step_end
	
GniessMovement2:
	walk_left
	walk_left
	face_down
	step_end
	
RivalMovement4:
	walk_down
	walk_down
	walk_down
	walk_down
	walk_down
	walk_down
	step_end
	
@ Thank You
ThankYouForTheOffer:
	lock
	faceplayer
	msgbox ThankYouForTheOffer_Text, 4
	closemessage
	release
	end
	
ThankYouForTheOffer_Text:
	.string "GNEISS: Thank you.$"

YouToo::
	lock
	faceplayer
	msgbox YouTooText, 4
	closemessage
	release
	end
	
YouTooText:
	.string "ALEX: He invited you too?\p"
	.string "Wonder what this is about.$"