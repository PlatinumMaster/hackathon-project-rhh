LittlerootTown_BrendansHouse_1F_MapScripts::
	map_script 2, LittlerootTown_BrendansHouse_1F_MapScriptType2
	.byte 0

// Type 2 Scripts
	
LittlerootTown_BrendansHouse_1F_MapScriptType2:
	map_script_2 VAR_0x4064, 0, LittlerootTown_BrendansHouse_1F_MapScript1_Call
	.2byte 0
	
// Script Calls

LittlerootTown_BrendansHouse_1F_MapScript1_Call:
	call LittlerootTown_BrendansHouse_1F_MapScript1_Data
	end
	
// Script Data

LittlerootTown_BrendansHouse_1F_MapScript1_Data:
	lockall
	playse 21
	applymovement 1, LittlerootTown_BrendansHouse_1F_Movement_Exclamation_Look_Right
	waitmovement 0
	delay 40
	applymovement 1, LittlerootTown_BrendansHouse_1F_Movement_Mom_Walks_To_You
	waitmovement 0
	delay 20
	msgbox LittlerootTown_BrendansHouse_1F_Text_Mom1, 4
	closemessage
	applymovement 1, LittlerootTown_BrendansHouse_1F_Movement_Mom_Walks_Back
	waitmovement 0
	setflag FLAG_0x112
	setflag FLAG_SYS_B_DASH
	setflag FLAG_0x052
	setflag FLAG_0x2D0
	clearflag FLAG_0x2D1
	setflag FLAG_0x2BC
	setvar VAR_0x4084, 2
	setvar VAR_0x4060, 3
	clearflag FLAG_SPECIAL_FLAG_0x4000
	setvar VAR_0x4064, 1
	release
	return
	
// Strings

LittlerootTown_BrendansHouse_1F_Text_Mom1:
	.string "Oh, {PLAYER}!\n"
	.string "The professor called for you earlier.\p"
	.string "But I told him you were asleep.\n"
	.string "You should go see what he wanted.$"

// Movements
LittlerootTown_BrendansHouse_1F_Movement_Exclamation_Look_Right:
	emote_exclamation_mark
	face_right
	step_end
	
LittlerootTown_BrendansHouse_1F_Movement_Mom_Walks_To_You:
	walk_right
	walk_right
	walk_right
	walk_up
	walk_up
	step_end
	
LittlerootTown_BrendansHouse_1F_Movement_Mom_Walks_Back:
	walk_down
	walk_down
	walk_left
	walk_left
	walk_left
	step_end

LittlerootTown_BrendansHouse_1F_Text_1F7A1C: @ 81F7A1C
	.string "MOM: See, {PLAYER}?\n"
	.string "Isn’t it nice in here, too?$"

LittlerootTown_BrendansHouse_1F_Text_1F7A46: @ 81F7A46
	.string "The mover’s POKéMON do all the work\n"
	.string "of moving us in and cleaning up after.\l"
	.string "This is so convenient!\p"
	.string "{PLAYER}, your room is upstairs.\n"
	.string "Go check it out, dear!\p"
	.string "DAD bought you a new clock to mark\n"
	.string "our move here.\l"
	.string "Don’t forget to set it!$"

LittlerootTown_BrendansHouse_1F_Text_1F7B24: @ 81F7B24
	.string "MOM: Well, {PLAYER}?\p"
	.string "Aren’t you interested in seeing your\n"
	.string "very own room?$"

LittlerootTown_BrendansHouse_1F_Text_1F7B67: @ 81F7B67
LittlerootTown_MaysHouse_1F_Text_1F7B67: @ 81F7B67
	.string "MOM: {PLAYER}.\p"
	.string "Go set the clock in your room, honey.$"

LittlerootTown_BrendansHouse_1F_Text_1F7B96: @ 81F7B96
	.string "MOM: Oh! {PLAYER}, {PLAYER}!\n"
	.string "Quick! Come quickly!$"

LittlerootTown_BrendansHouse_1F_Text_1F7BBC: @ 81F7BBC
LittlerootTown_MaysHouse_1F_Text_1F7BBC: @ 81F7BBC
	.string "MOM: Look! It’s PETALBURG GYM!\n"
	.string "Maybe DAD will be on!$"

LittlerootTown_BrendansHouse_1F_Text_1F7BF1: @ 81F7BF1
LittlerootTown_MaysHouse_1F_Text_1F7BF1: @ 81F7BF1
	.string "MOM: Oh… It’s over.\p"
	.string "I think DAD was on, but we missed him.\n"
	.string "Too bad.$"

LittlerootTown_BrendansHouse_1F_Text_1F7C35: @ 81F7C35
LittlerootTown_MaysHouse_1F_Text_1F7C35: @ 81F7C35
	.string "Oh, yes.\n"
	.string "One of DAD’s friends lives in town.\p"
	.string "PROF. BIRCH is his name.\p"
	.string "He lives right next door, so you should\n"
	.string "go over and introduce yourself.$"

LittlerootTown_BrendansHouse_1F_Text_1F7CC3: @ 81F7CC3
	.string "MOM: See you, honey!$"

LittlerootTown_BrendansHouse_1F_Text_1F7CD8: @ 81F7CD8
	.string "MOM: Did you introduce yourself to\n"
	.string "PROF. BIRCH?$"

LittlerootTown_BrendansHouse_1F_Text_1F7D08: @ 81F7D08
	.string "MOM: How are you doing, {PLAYER}?\n"
	.string "You look a little tired.\p"
	.string "I think you should rest a bit.$"

LittlerootTown_BrendansHouse_1F_Text_1F7D5C: @ 81F7D5C
	.string "MOM: Take care, honey!$"

LittlerootTown_BrendansHouse_1F_Text_1F7D73: @ 81F7D73
	.string "MOM: Oh? Did DAD give you that BADGE?\p"
	.string "Then here’s something from your MOM!$"

LittlerootTown_BrendansHouse_1F_Text_1F7DBE: @ 81F7DBE
	.string "Don’t push yourself too hard, dear.\n"
	.string "You can always come home.\p"
	.string "Go for it, honey!$"

LittlerootTown_BrendansHouse_1F_Text_1F7E0E: @ 81F7E0E
	.string "MOM: What is that, honey? A POKéNAV?\n"
	.string "Someone from DEVON gave it to you?\p"
	.string "Well, honey, how about registering\n"
	.string "your mom?\p"
	.string "… … …$"

LittlerootTown_BrendansHouse_1F_Text_1F7E89: @ 81F7E89
	.string "Registered MOM\n"
	.string "in the POKéNAV.$"

LittlerootTown_BrendansHouse_1F_Text_1F7EA8: @ 81F7EA8
	.string "Fugiiiiih!$"

LittlerootTown_BrendansHouse_1F_Text_1F7EB3: @ 81F7EB3
	.string "Huggoh, uggo uggo…$"

LittlerootTown_BrendansHouse_1F_Text_1F7EC6: @ 81F7EC6
	.string "INTERVIEWER: …We brought you this\n"
	.string "report from in front of PETALBURG GYM.$"

LittlerootTown_BrendansHouse_1F_Text_1F7F0F: @ 81F7F0F
	.string "There is a movie on TV.\p"
	.string "Two men are dancing on a big piano\n"
	.string "keyboard.\p"
	.string "Better get going!$"

LittlerootTown_BrendansHouse_1F_Text_1F7F66: @ 81F7F66
	.string "It’s the instruction booklet for the\n"
	.string "RUNNING SHOES.\p"
	.string "“Press the B Button to run while\n"
	.string "wearing your RUNNING SHOES.\p"
	.string "“Lace up your RUNNING SHOES and hit\n"
	.string "the road running!”$"

LittlerootTown_BrendansHouse_1F_Text_1F800E: @ 81F800E
	.string "DAD: Hm?\p"
	.string "Hey, it’s {PLAYER}!\p"
	.string "It’s been a while since I saw you,\n"
	.string "but you look…stronger, somehow.\p"
	.string "That’s the impression I get.\n"
	.string "But your old man hasn’t given up yet!\p"
	.string "Oh, yes, I have something for you.\n"
	.string "This came to you from someone named\l"
	.string "MR. BRINEY.$"

LittlerootTown_BrendansHouse_1F_Text_1F80FE: @ 81F80FE
	.string "DAD: Hm, a TICKET for a ferry?\p"
	.string "If I recall, there are ferry ports in\n"
	.string "SLATEPORT and LILYCOVE.$"

LittlerootTown_BrendansHouse_1F_Text_1F815B: @ 81F815B
	.string "I’d better get back to PETALBURG GYM.\p"
	.string "MOM, thanks for looking after the house\n"
	.string "while I’m away.$"

LittlerootTown_BrendansHouse_1F_Text_1F81B9: @ 81F81B9
	.string "MOM: That DAD of yours…\p"
	.string "He comes home for the first time in a\n"
	.string "while, but all he talks about is POKéMON.\p"
	.string "He should relax and stay a little longer.$"

LittlerootTown_BrendansHouse_1F_Text_1F824B: @ 81F824B
	.string "MOM: Is that a breaking news story?$"

LittlerootTown_BrendansHouse_1F_Text_1F826F: @ 81F826F
	.string "We bring you this emergency\n"
	.string "news flash!\p"
	.string "In various HOENN locales, there have\n"
	.string "been reports of a BZZT…colored\l"
	.string "POKéMON in flight.\p"
	.string "The identity of this POKéMON is\n"
	.string "currently unknown.\p"
	.string "We now return you to the regular\n"
	.string "movie program.$"

LittlerootTown_BrendansHouse_1F_Text_1F8351: @ 81F8351
	.string "MOM: {PLAYER}, did you catch that?\p"
	.string "What color did the announcer say\n"
	.string "that POKéMON was?$"

LittlerootTown_BrendansHouse_1F_Text_1F83A1: @ 81F83A1
	.string "MOM: Well, isn’t that something!\n"
	.string "There are still unknown POKéMON.$"
