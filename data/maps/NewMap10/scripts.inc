NewMap10_MapScripts::
	.byte 0
	
GymSign1:
	msgbox GYMSIGN, 2
	end
	
GYMSIGN:
	.string "CORONADO CITY GYM\n"
	.string "LEADER: FROSTES$"
	
Gym1Dude:
	checkflag FLAG_0x024
	goto_if 1, YoContinued
	msgbox Yo, 2
	end
	
Yo:
	.string "Yo! Think you can beat FROSTES?\n"
	.string "It’ll be an icy sting!$"
	
YoContinued:
	msgbox Yo2, 2
	end
	
Yo2:
	.string "Woah! You did it!\n"
	.string "You beat FROSTES!$"
	
GymLeaderFrostes::
	lock
	checkflag FLAG_0x024
	goto_if 1,  GymLeaderFrostesDone
	faceplayer
	msgbox FrostesText1, 4
	closemessage
	delay 20
	trainerbattle 1, TRAINER_ROXANNE_1, 0, FrostesText3, FrostesText4, FrostesContinued
	
FrostesContinued::
	lock
	faceplayer
	msgbox FrostesText5, 4
	closemessage
	delay 20
	msgbox FrostesText6, 4
	closemessage
	delay 20
	msgbox RecievedBadge, 4
	closemessage
	delay 20
	msgbox FrostesText7, 4
	closemessage
	giveitem_std ITEM_TM39
	waitmessage
	msgbox FrostesText8, 4
	setflag FLAG_BADGE01_GET
	setflag FLAG_0x024
	release
	end

	GymLeaderFrostesDone:
	lock
	faceplayer
	msgbox FrostesText9, 4
	closemessage
	release
	end
	
FrostesText1:
	.string "FROSTES: Well! Look what we have here.\n"
	.string "If it isn’t a new challenger.$"
	
FrostesText3:
	.string "What a bore.\n"
	.string "No one likes challenging me.\p"
	.string "I don’t blame them.\n"
	.string "Most trainers have trouble against\l"
	.string "my precious POKéMON.\p"
	.string "No doubt about that, darling.\p"
	.string "…What? Did you just ask whether\n"
	.string "I’m a girl?\p"
	.string "You’re annoying.\n"
	.string "Just go join the other deadbeats!$"
	
FrostesText4:
	.string "… … …\n"
	.string "… … …$"
	
FrostesText5:
	.string "FROSTES: Heh.\n"
	.string "You’re not bad at all.\p"
	.string "It’s been a while since I’ve had that\n"
	.string "much fun!$"

FrostesText6:
	.string "Well, then…\n"
	.string "As the league rules state…\p"
	.string "I bestow upon you the Crystal Badge!$"
	
RecievedBadge:
	.string "{PLAYER} recieved the CRYSTAL BADGE!$"
	
FrostesText7:
	.string "Here, take this as well.$"
	
FrostesText8:
	.string "This TM, or technical machine, contains\n"
	.string "the move ICE BEAM.\p"
	.string "It will blast the foe with an icy cold\n"
	.string "beam that may freeze the target.\p"
	.string "Amazing, right?\n$"
	
FrostesText9:
	.string "FROSTES: I’d like to have a rematch\n"
	.string "someday!$"

	
	
