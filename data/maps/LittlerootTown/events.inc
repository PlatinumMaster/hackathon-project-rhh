LittlerootTown_EventObjects::
	object_event 1, EVENT_OBJ_GFX_SPEAROW_1, 0, 12, 13, 15, MOVEMENT_TYPE_FACE_UP, 0, 0, FALSE, 0, NULL, FLAG_0x020
	object_event 2, EVENT_OBJ_GFX_SPEAROW_1, 0, 13, 14, 15, MOVEMENT_TYPE_FACE_UP, 0, 0, FALSE, 0, NULL, FLAG_0x020
	object_event 3, EVENT_OBJ_GFX_SPEAROW_1, 0, 10, 15, 15, MOVEMENT_TYPE_FACE_UP, 0, 0, FALSE, 0, NULL, FLAG_0x020
	object_event 4, EVENT_OBJ_GFX_OLD_MAN_1, 0, 39, 17, 3, MOVEMENT_TYPE_FACE_LEFT, 0, 0, FALSE, 0, WhatsUpOldMan, FLAG_0x026
	object_event 5, EVENT_OBJ_GFX_FAT_MAN, 0, 21, 18, 3, MOVEMENT_TYPE_WANDER_AROUND, 0, 0, FALSE, 0, NULL, 0
	object_event 6, EVENT_OBJ_GFX_GIRL_2, 0, 33, 25, 3, MOVEMENT_TYPE_LOOK_AROUND, 0, 0, FALSE, 0, NULL, 0
	object_event 7, EVENT_OBJ_GFX_PIKACHU, 0, 11, 9, 3, MOVEMENT_TYPE_LOOK_AROUND, 0, 0, FALSE, 0, NULL, 0
	object_event 8, EVENT_OBJ_GFX_AZUMARILL, 0, 13, 8, 3, MOVEMENT_TYPE_LOOK_AROUND, 0, 0, FALSE, 0, NULL, 0

LittlerootTown_MapWarps::
	warp_def 11, 16, 0, 1, MAP_LITTLEROOT_TOWN_BRENDANS_HOUSE_1F
	warp_def 26, 16, 0, 0, MAP_NEW_MAP5
	warp_def 19, 10, 3, 0, MAP_LITTLEROOT_TOWN_PROFESSOR_BIRCHS_LAB
	warp_def 17, 12, 3, 0, MAP_NEW_MAP10

LittlerootTown_MapCoordEvents::
	coord_event 39, 18, 3, VAR_0x40FC, 0, HeyNotAllowedToGoPastBottom
	coord_event 39, 16, 3, VAR_0x40FC, 0, HeyNotAllowedToGoPastTop

LittlerootTown_MapBGEvents::
	bg_event 22, 10, 3, BG_EVENT_PLAYER_FACING_ANY, labhouse
	bg_event 10, 17, 3, BG_EVENT_PLAYER_FACING_ANY, playerhouse
	bg_event 27, 24, 3, BG_EVENT_PLAYER_FACING_ANY, townsign
	bg_event 23, 24, 3, BG_EVENT_PLAYER_FACING_ANY, Doorlocked

LittlerootTown_MapEvents::
	map_events LittlerootTown_EventObjects, LittlerootTown_MapWarps, LittlerootTown_MapCoordEvents, LittlerootTown_MapBGEvents
