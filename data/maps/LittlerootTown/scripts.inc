LittlerootTown_MapScripts::
	map_script 2, LittlerootTown_MapScriptType2
	map_script 4, LittlerootTown_MapScriptType4
	.byte 0

// Type 2 Scripts
	
LittlerootTown_MapScriptType2:
	map_script_2 VAR_0x4063, 0, LittlerootTown_MapScript1_Call
	.2byte 0
	
// Type 4 Scripts

LittlerootTown_MapScriptType4:
	map_script_2 VAR_0x4064, 0, LittlerootTown_MapScript2_Call
	.2byte 0
	
// Script Calls

LittlerootTown_MapScript1_Call:
	call LittlerootTown_MapScript1_Data
	end
	
LittlerootTown_MapScript2_Call:
	call LittlerootTown_MapScript2_Data
	end
	
// Script Data

LittlerootTown_MapScript1_Data:
	setrespawn HEAL_LOCATION_LITTLEROOT_TOWN_1
	resetweather
	doweather
	playmoncry SPECIES_SPEAROW, 2
	delay 14
	playmoncry SPECIES_SPEAROW, 5
	applymovement 1, LittlerootTownMovement_Spearow1
	applymovement 2, LittlerootTownMovement_Spearow1
	applymovement 3, LittlerootTownMovement_Spearow1
	waitmovement 0
	setflag FLAG_0x020
	setvar VAR_0x4063, 1
	release
	return
	
LittlerootTown_MapScript2_Data:
	setweather 8
	doweather
	setvar VAR_0x4064, 1
	return
	
HeyNotAllowedToGoPastTop::
	checkflag FLAG_0x026
	call_if 1, Nothing
	lock
	playse 21
	applymovement 4, exclamation
	waitmovement 0
	applymovement 4, Faceup
	waitmovement 0
	msgbox DontGo, 4
	closemessage
	applymovement 255, Walkleft
	waitmovement 0
	release
	end

HeyNotAllowedToGoPastBottom::
	checkflag FLAG_0x026
	call_if 1, Nothing
	lock
	playse 21
	applymovement 4, exclamation
	waitmovement 0
	applymovement 4, Facedown
	waitmovement 0
	msgbox DontGo, 4
	closemessage
	applymovement 255, Walkleft
	waitmovement 0
	release
	end

Nothing::
	end
	
WhatsUpOldMan::
	lock
	faceplayer
	msgbox WhatsUpText, 4
	closemessage
	release
	end
	
Doorlocked::
	lock
	msgbox doorlockedtext, 4
	closemessage
	release
	end
	
playerhouse::
	lock
	msgbox playerhousetext, 4
	closemessage
	release
	end
	
rivalhouse::
	lock
	msgbox rivalhousetext, 4
	closemessage
	release
	end
	
labhouse::
	lock
	msgbox labhousetext, 4
	closemessage
	release
	end
	
townsign::
	lock
	msgbox townsigntext, 4
	closemessage
	release
	end

// Strings

playerhousetext:
	.string "{PLAYER}’s house$"
	
rivalhousetext:
	.string "ALEX’s house$"
	
labhousetext:
	.string "PROF. GNEISS’ laboratory$"
	
townsigntext:
	.string "PIETER SITE\n"
	.string "The home of research.$"

DontGo::
	.string "Hey, {PLAYER}!\n"
	.string "You can’t go out there.\l"
	.string "You don’t have any POKéMON!$"
	
doorlockedtext:
	.string "The door seems to be locked…$"
	
WhatsUpText::
	.string "Hey, {PLAYER}!\n"
	.string "Today’s a great day!$"
	
// Movements

exclamation:
	emote_exclamation_mark
	step_end

Faceup:
	face_up
	step_end
	
Facedown:
	face_down
	step_end

Walkleft:
	walk_left
	step_end

LittlerootTownMovement_Spearow1:
	face_left
	walk_diag_southwest
	walk_diag_southwest
	walk_diag_southwest
	walk_diag_southwest
	walk_diag_southwest
	walk_diag_southwest
	walk_diag_southwest
	walk_diag_southwest
	walk_diag_southwest
	walk_diag_southwest
	walk_diag_southwest
	walk_diag_southwest
	walk_diag_southwest
	walk_diag_southwest
	step_end
