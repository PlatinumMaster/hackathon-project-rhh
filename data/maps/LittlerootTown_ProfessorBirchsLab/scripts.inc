LittlerootTown_ProfessorBirchsLab_MapScripts:: @ 81F9C91
	map_script 2, BirchScript
	.byte 0

BirchScript:
	map_script_2 VAR_0x40FF, 0, YouMustBeHereToSeeProf
	.2byte 0
	
YouMustBeHereToSeeProf::
	lock
	delay 20
	applymovement 255, YouMustBeHereToSeeProf_Movement
	waitmovement 0
	msgbox YouMustBeHereToSeeProf_Text, 4
	setvar VAR_0x40FF, 1
	release
	end
	
DidYouCheckUpstairs::
	lock
	faceplayer
	msgbox DidYouCheckUpstairs_Text, 4
	release
	end
	
DidYouCheckUpstairs_Text:
	.string "Did you check upstairs?"
	.string "$"
	
YouMustBeHereToSeeProf_Text:
	.string "Who might you be?\p… … …{PAUSE 20}\p"
	.string "Oh, you must be the kid Gniess was\n"
	.string "talking about.\p"
	.string "I believe he’s upstairs.$"
	
YouMustBeHereToSeeProf_Movement:
		walk_up
		walk_up
		walk_left
		step_end