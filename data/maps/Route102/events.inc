Route102_EventObjects::
	object_event 1, EVENT_OBJ_GFX_CAMPER, 0, 15, 28, 3, MOVEMENT_TYPE_FACE_LEFT, 0, 0, TRUE, 6, TrainerHarvey, FLAG_0x90F
	object_event 2, EVENT_OBJ_GFX_ITEM_BALL, 0, 16, 16, 3, MOVEMENT_TYPE_LOOK_AROUND, 0, 0, FALSE, 0, NULL, 0
	object_event 3, EVENT_OBJ_GFX_RIVAL_MAY_NORMAL, 0, 11, 37, 3, MOVEMENT_TYPE_FACE_UP, 0, 0, FALSE, 0, NULL, FLAG_0x02A
	object_event 4, EVENT_OBJ_GFX_BRENDAN_NORMAL, 0, 11, 37, 3, MOVEMENT_TYPE_FACE_UP, 0, 0, FALSE, 0, NULL, FLAG_0x029
	object_event 5, EVENT_OBJ_GFX_HIKER, 0, 9, 19, 3, MOVEMENT_TYPE_WANDER_AROUND, 0, 0, FALSE, 0, Hiker1, 0
	object_event 6, EVENT_OBJ_GFX_CAMPER, 0, 36, 19, 3, MOVEMENT_TYPE_WANDER_AROUND, 0, 0, FALSE, 0, camperdude, 0
	object_event 7, EVENT_OBJ_GFX_BOY_2, 0, 39, 41, 3, MOVEMENT_TYPE_LOOK_AROUND, 0, 0, FALSE, 0, canyonboy, 0
	object_event 8, EVENT_OBJ_GFX_HIKER, 0, 27, 60, 3, MOVEMENT_TYPE_FACE_DOWN, 0, 0, FALSE, 0, HikerBlocking, FLAG_0x023
	object_event 9, EVENT_OBJ_GFX_SCIENTIST_1, 0, 37, 5, 3, MOVEMENT_TYPE_FACE_UP, 0, 0, FALSE, 4, ProfBlockingCave2, 0
	object_event 10, EVENT_OBJ_GFX_HIKER, 0, 34, 33, 3, MOVEMENT_TYPE_FACE_LEFT, 0, 0, TRUE, 4, TrainerHikerOnTheRight, 0
	object_event 11, EVENT_OBJ_GFX_MAY_NORMAL, 0, 49, 31, 3, MOVEMENT_TYPE_NONE, 0, 0, FALSE, 0, LookingOver, FLAG_0x031
	object_event 12, EVENT_OBJ_GFX_BRENDAN_NORMAL, 0, 37, 13, 3, MOVEMENT_TYPE_NONE, 0, 0, FALSE, 0, LookingOver, FLAG_0x030
	object_event 13, EVENT_OBJ_GFX_MAY_NORMAL, 0, 37, 13, 3, MOVEMENT_TYPE_NONE, 0, 0, TRUE, 3, LookingOver, FLAG_0x031
	object_event 14, EVENT_OBJ_GFX_BLACK_BELT, 0, 29, 4, 3, MOVEMENT_TYPE_FACE_DOWN_AND_LEFT, 0, 0, TRUE, 3, karatetrainer, 0

Route102_MapWarps::
	warp_def 7, 49, 3, 1, MAP_NEW_MAP2
	warp_def 37, 4, 3, 0, MAP_NEW_MAP6

Route102_MapCoordEvents::
	coord_event 10, 40, 3, VAR_0x40F6, 0, Left
	coord_event 11, 40, 3, VAR_0x40F6, 0, Middle
	coord_event 12, 40, 3, VAR_0x40F6, 0, Right
	coord_event 29, 60, 3, VAR_0x4061, 0, HikerBlockingRightRight
	coord_event 25, 60, 3, VAR_0x4061, 0, HikerBlockingLeftLeft
	coord_event 28, 60, 3, VAR_0x4061, 0, HikerBlockingRight
	coord_event 26, 60, 3, VAR_0x4061, 0, HikerBlockingLeft

Route102_MapEvents::
	map_events Route102_EventObjects, Route102_MapWarps, Route102_MapCoordEvents, 0x0
