Route102_MapScripts:: @ 81EC0E0
	map_script 2, RivalGenderCheckMapScriptType
	.byte 0
	
RivalGenderCheckMapScriptType: @ 81EBCCB
	map_script_2 VAR_0x40F5, 0, RivalGenderCheck
	.2byte 0
	
LookingOver::
	lock
	faceplayer
	msgbox woahwoah, 4
	applymovement 12, LookDown2
	waitmovement 0
	applymovement 13, LookDown2
	waitmovement 0
	release
	end
	
woahwoah:
	.string "{PLAYER}!\n"
	.string "I’m just healing my POKeMON.\p"
	.string "Talk to the scientist in front\n"
	.string "of the cave.$"
	
ProfBlockingCave2::
	checkflag FLAG_0x023
	goto_if 1, Done22
	checkplayergender
	compare VAR_RESULT, 0 // If rival is female
	goto_eq FEMALEPROFEVENT
	compare VAR_RESULT, 1 // If rival is male
	goto_eq MALEPROFEVENT
	end
	
RivalGenderCheck:
	checkplayergender
	compare VAR_RESULT, 0 // If rival is female
	goto_eq FEMALE6
	compare VAR_RESULT, 1 // If rival is male
	goto_eq MALE6
	
FEMALE6:
	removeobject 12
	setvar VAR_0x40F5, 1
	end
	
MALE6:
	removeobject 13
	setvar VAR_0x40F5, 1
	end
	
FEMALEPROFEVENT:
	removeobject 12
	specialvar VAR_RESULT, CheckLileep
	compare VAR_RESULT, 1
	goto_eq RivalAnorithFemale
	goto RivalLileepFemale
	
MALEPROFEVENT:
	removeobject 13
	specialvar VAR_RESULT, CheckAnorith
	compare VAR_RESULT, 1
	goto_eq RivalLileepMale
	goto RivalAnorithMale
	
RivalLileepFemale::
	lock
	faceplayer
	showobjectat 13, MAP_ROUTE102
	msgbox NoCantEnter, 4
	closemessage
	delay 20
	msgbox RivalCallsPlayer, 4
	closemessage
	delay 20
	applymovement 13, WalkUpToProfandPlayer
	waitmovement 0
	applymovement 12, WalkUpToProfandPlayer
	waitmovement 0
	applymovement 255 LookDown2
	waitmovement 0
	delay 20
	msgbox whydontwebattle, 4
	closemessage
	delay 20
	trainerbattle 1, TRAINER_GRUNT_3, 0, RivalBefore, RivalDefeat, RivalContinued

RivalAnorithFemale::
	lock
	faceplayer
	showobjectat 13, MAP_ROUTE102
	msgbox NoCantEnter, 4
	closemessage
	delay 20
	msgbox RivalCallsPlayer, 4
	closemessage
	delay 20
	applymovement 13, WalkUpToProfandPlayer
	waitmovement 0
	applymovement 12, WalkUpToProfandPlayer
	waitmovement 0
	applymovement 255 LookDown2
	waitmovement 0
	delay 20
	msgbox whydontwebattle, 4
	closemessage
	delay 20
	trainerbattle 1, TRAINER_GRUNT_4, 0, RivalBefore, RivalDefeat, RivalContinued

RivalLileepMale::
	lock
	faceplayer
	showobjectat 12, MAP_ROUTE102
	msgbox NoCantEnter, 4
	closemessage
	delay 20
	msgbox RivalCallsPlayer, 4
	closemessage
	delay 20
	applymovement 12, WalkUpToProfandPlayer
	waitmovement 0
	applymovement 13, WalkUpToProfandPlayer
	waitmovement 0
	applymovement 255 LookDown2
	waitmovement 0
	delay 20
	msgbox whydontwebattle, 4
	closemessage
	delay 20
	trainerbattle 1, TRAINER_GRUNT_5, 0, RivalBefore, RivalDefeat, RivalContinued

RivalAnorithMale::
	lock
	faceplayer
	showobjectat 12, MAP_ROUTE102
	applymovement 12, Show
	msgbox NoCantEnter, 4
	closemessage
	delay 20
	msgbox RivalCallsPlayer, 4
	closemessage
	delay 20
	applymovement 12, WalkUpToProfandPlayer
	waitmovement 0
	applymovement 13, WalkUpToProfandPlayer
	waitmovement 0
	applymovement 255 LookDown2
	waitmovement 0
	delay 20
	msgbox whydontwebattle, 4
	closemessage
	delay 20
	trainerbattle 1, TRAINER_GRUNT_6, 0, RivalBefore, RivalDefeat, RivalContinued
	
RivalContinued::
	delay 20
	msgbox woahgood, 4
	closemessage
	delay 20
	applymovement 13 walkawaydown
	waitmovement 0
	applymovement 12 walkawaydown
	waitmovement 0
	playse 21
	applymovement 13 exclamation
	waitmovement 0
	applymovement 12 exclamation
	waitmovement 0
	delay 20
	applymovement 13 walkawayup
	waitmovement 0
	applymovement 12 walkawayup
	waitmovement 0
	msgbox rivalialmostforget, 4
	closemessage
	giveitem_std ITEM_POKE_BALL, 5
	closemessage
	delay 20
	applymovement 13 walkawaydown2
	waitmovement 0
	applymovement 12 walkawaydown2
	waitmovement 0
	hideobjectat 13, MAP_ROUTE102
	hideobjectat 12, MAP_ROUTE102
	removeobject 13
	removeobject 12
	setflag FLAG_0x023
	setvar VAR_0x4061, 1
	release
	end
	
rivalialmostforget:
	.string "ALEX: I almost forgot!\n"
	.string "These should be useful!\l"
	.string "Go catch some cool POKéMON, okay?$"

Done22::
	lock
	faceplayer
	msgbox NoCantEnter, 4
	closemessage
	release
	end
	
NoCantEnter:
	.string "Hey, {PLAYER}.\n"
	.string "GNEISS told about everything.\p"
	.string "Unfortunately, there’s a problem.\n"
	.string "An emergency happened, so we can’t\l"
	.string "let you inside.$"
	
RivalCallsPlayer:
	.string "{PLAYER}!\n"
	.string "You made it!$"
	
whydontwebattle:
	.string "ALEX: Huh?\n"
	.string "So we can’t go in…\p"
	.string "I’ll probably head to CORONADO CITY.\n"
	.string "It’s to the south of the canyon.\p"
	.string "But before that.$"
	
RivalBefore:
	.string "What do you say to a battle?$"
	
RivalDefeat:
	.string "Woah! I didn’t expect that.$"
	
WalkUpToProfandPlayer:
	walk_up
	walk_up
	walk_up
	walk_up
	walk_up
	walk_up
	step_end
	
woahgood:
	.string "ALEX: You’re a pretty good, {PLAYER}!\n"
	.string "I’ll be heading off now.$"
	
walkawaydown:
	walk_down
	walk_down
	walk_down
	walk_down
	step_end

walkawayup:
	walk_up
	walk_up
	walk_up
	walk_up
	step_end
	
walkawaydown2:
	walk_down
	walk_down
	walk_down
	walk_down
	walk_down
	walk_down
	walk_down
	walk_down
	step_end

Left::
	lock
	delay 20
	special SpawnScriptEventObject
	applymovement 127, MoveCameraUpLeft
	waitmovement 0
	delay 20
	applymovement 4, LookDown2
	waitmovement 0
	applymovement 3, LookDown2
	waitmovement 0
	msgbox ComeOnText, 4
	closemessage
	delay 20
	applymovement 127, MoveCameraBackLeft
	applymovement 4, WalkDownToPlayerLeft
	waitmovement 0
	applymovement 3, WalkDownToPlayerLeft
	waitmovement 0
	delay 20
	msgbox BewareTrainers, 4
	closemessage
	delay 20
	special RemoveScriptEventObject
	applymovement 4, WalkOff
	waitmovement 0
	applymovement 3, WalkOff
	waitmovement 0
	delay 20
	removeobject 4
	removeobject 3
	setvar VAR_0x40F6, 1
	setflag FLAG_0x02C
	release
	end

Middle::
	lock
	delay 20
	special SpawnScriptEventObject
	applymovement 127, MoveCameraUpMiddle
	waitmovement 0
	delay 20
	applymovement 4, LookDown2
	waitmovement 0
	applymovement 3, LookDown2
	waitmovement 0
	msgbox ComeOnText, 4
	closemessage
	delay 20
	applymovement 127, MoveCameraBackMiddle
	applymovement 4, WalkDownToPlayerMiddle
	waitmovement 0
	applymovement 3, WalkDownToPlayerMiddle
	waitmovement 0
	delay 20
	msgbox BewareTrainers, 4
	closemessage
	delay 20
	special RemoveScriptEventObject
	applymovement 4, WalkOff
	waitmovement 0
	applymovement 3, WalkOff
	waitmovement 0
	delay 20
	removeobject 4
	removeobject 3
	setvar VAR_0x40F6, 1
	setflag FLAG_0x02C
	release
	end

Right::
	lock
	delay 20
	special SpawnScriptEventObject
	applymovement 127, MoveCameraUpRight
	waitmovement 0
	delay 20
	applymovement 4, LookDown2
	waitmovement 0
	applymovement 3, LookDown2
	waitmovement 0
	msgbox ComeOnText, 4
	closemessage
	delay 20
	applymovement 127, MoveCameraBackRight
	applymovement 4, WalkDownToPlayerRight
	waitmovement 0
	applymovement 3, WalkDownToPlayerRight
	waitmovement 0
	delay 20
	msgbox BewareTrainers, 4
	closemessage
	delay 20
	special RemoveScriptEventObject
	applymovement 4, WalkOff
	waitmovement 0
	applymovement 3, WalkOff
	waitmovement 0
	delay 20
	removeobject 4
	removeobject 3
	setvar VAR_0x40F6, 1
	setflag FLAG_0x02C
	release
	end
	
Hide:
	set_invisible
	step_end
	
Show:
	set_visible
	step_end	

MoveCameraUpLeft:
	levitate
	walk_up
	walk_up
	walk_up
	walk_right
	stop_levitate
	step_end

MoveCameraUpMiddle:
	levitate
	walk_up
	walk_up
	walk_up
	stop_levitate
	step_end
	
MoveCameraUpRight:
	levitate
	walk_up
	walk_up
	walk_up
	walk_left
	stop_levitate
	step_end
	
LookDown2:
	face_down
	step_end
	
ComeOnText:
	.string "ALEX: You’re quick, huh?$"
	
MoveCameraBackLeft:
	levitate
	walk_down
	walk_down
	walk_down
	walk_left
	stop_levitate
	step_end
	
MoveCameraBackMiddle:
	levitate
	walk_down
	walk_down
	walk_down
	stop_levitate
	step_end
	
MoveCameraBackRight:
	levitate
	walk_down
	walk_down
	walk_down
	walk_right
	stop_levitate
	step_end
	
WalkDownToPlayerLeft:	
	walk_down
	walk_down
	walk_left
	face_down
	step_end
	
WalkDownToPlayerMiddle:	
	walk_down
	walk_down
	face_down
	step_end
	
WalkDownToPlayerRight:	
	walk_down
	walk_down
	walk_right
	face_down
	step_end
	
BewareTrainers:
	.string "ALEX: Be careful.\n"
	.string "There’s trainers around!\l"
	.string "Be prepared for a battle!$"
	
WalkOff:
	walk_up
	walk_up
	walk_up
	walk_up
	walk_up
	walk_up
	walk_up
	step_end
	
TrainerHarvey::
	trainerbattle 0, TRAINER_SAWYER_1, 0, EncounterHarveyText, DefeatHarveyText
	msgbox AfterBattleHarvey, 6
	end
	
EncounterHarveyText:
	.string "This POKéMON seems fast!\p"
	.string "Check it out!$"
	
DefeatHarveyText:
	.string "Huh? I guess it wasn’t fast enough…$"
	
AfterBattleHarvey:
	.string "I’ve heard unique POKéMON make their\n"
	.string "homes in this canyon.$"
	
karatetrainer::
	trainerbattle 0, TRAINER_GRUNT_1, 0, encounterkaratetrainer, defeatkaratetrainer
	msgbox afterkaratetrainer, 6
	end
	
encounterkaratetrainer:
	.string "Huff…\n"
	.string "Maybe if I beat you, I’ll have the\l"
	.string "strength to destroy this rock!$"
	
defeatkaratetrainer:
	.string "No. I can’t give up!$"
	
afterkaratetrainer:
	.string "One day, I’ll destroy this rock.\n"
	.string "That’ll show you!$"
	
TrainerHikerOnTheRight::
	trainerbattle 0, TRAINER_GRUNT_2, 0, EncounterHikerOnTheRight, DefeatHikerOnTheRight
	msgbox AfterHikerOnTheRight, 6
	end
	
EncounterHikerOnTheRight:
	.string "Yo! Did you know that this canyon\n"
	.string "is famous for its vibrant colors?$"
	
DefeatHikerOnTheRight:
	.string "EKANS!$"
	
AfterHikerOnTheRight:
	.string "As a kid, I always dreamed of visiting\n"
	.string "this canyon. Now, I’m finally here!$"

Hiker1::
	lock
	faceplayer
	msgbox Hiker1Text, 4
	closemessage
	release
	end
	
Hiker1Text:
	.string "Darn! I dropped a cool rock.\n"
	.string "Hey, don’t pick it up, alright?$"
	
camperdude::
	lock
	faceplayer
	msgbox camperdudetext, 4
	closemessage
	release
	end
	
camperdudetext:
	.string "There’s a town to the north.\n"
	.string "But some pesky POKÉMON are blocking\l"
	.string "the path!$"
	
canyonboy::
	lock
	faceplayer
	msgbox canyonboytext, 4
	closemessage
	release
	end
	
canyonboytext:
	.string "The grass here seems so dry…\n"
	.string "It’s the perfect environment for\l"
	.string "SPEAROW.$"
	
HikerBlocking::
	msgbox HikerBlockingText, 2
	end
	
HikerBlockingText:
	.string "Sorry!\n"
	.string "I can’t let you through!\p"
	.string "I dropped my ITEMFINDER!$"
	
HikerBlockingLeftLeft::
	lock
	playse 21
	applymovement 8, exclamation
	waitmovement 0
	delay 20
	applymovement 8, HikerWalkLeft
	waitmovement 0
	msgbox HikerBlockingScriptText
	closemessage
	applymovement 8, WalkBackLeftLeft
	applymovement 255, WalkUp
	waitmovement 0
	delay 20
	release
	end

HikerBlockingLeft::
	lock
	playse 21
	applymovement 8, exclamation
	waitmovement 0
	delay 20
	applymovement 8, HikerLookLeft
	waitmovement 0
	msgbox HikerBlockingScriptText
	closemessage
	applymovement 8, LookDown2
	applymovement 255, WalkUp
	waitmovement 0
	delay 20
	release
	end

HikerBlockingRight::
	lock
	playse 21
	applymovement 8, exclamation
	waitmovement 0
	delay 20
	applymovement 8, HikerLookRight
	waitmovement 0
	msgbox HikerBlockingScriptText
	closemessage
	applymovement 8, WalkBackRightRight
	applymovement 255, WalkUp
	waitmovement 0
	delay 20
	release
	end

HikerBlockingRightRight::
	lock
	playse 21
	applymovement 8, exclamation
	waitmovement 0
	delay 20
	applymovement 8, HikerWalkRight
	waitmovement 0
	msgbox HikerBlockingScriptText
	closemessage
	applymovement 8, LookDown2
	applymovement 255, WalkUp
	waitmovement 0
	delay 20
	release
	end
	
WalkUp:
	walk_up
	step_end
	
HikerWalkLeft:
	walk_left
	step_end
	
HikerLookLeft:
	face_left
	step_end

HikerWalkRight:
	walk_right
	step_end
	
HikerLookRight:
	face_right
	step_end
	
WalkBackLeftLeft:
	walk_right
	face_down
	step_end

WalkBackRightRight:
	walk_left
	face_down
	step_end
	
HikerBlockingScriptText:
	.string "Hiiyah!\n"
	.string "I can’t let you through!\p"
	.string "I dropped my ITEMFINDER!$"
	
PetalburgCity_Text_1EC1F8: @ 81EC1F8
	.string "WALLY: {PLAYER}…\n"
	.string "POKéMON hide in tall grass like this,\l"
	.string "don’t they?\p"
	.string "Please watch me and see if I can\n"
	.string "catch one properly.\p"
	.string "…Whoa!$"

PetalburgCity_Text_1EC271: @ 81EC271
	.string "WALLY: I did it… It’s my…\n"
	.string "My POKéMON!$"

PetalburgCity_Text_1EC297: @ 81EC297
	.string "{PLAYER}, thank you!\n"
	.string "Let’s go back to the GYM!$"

Route102_Text_1EC2C0: @ 81EC2C0
	.string "I’m…not very tall, so I sink right\n"
	.string "into tall grass.\p"
	.string "The grass goes up my nose and…\n"
	.string "Fwafwafwafwafwa…\p"
	.string "Fwatchoo!$"

Route102_Text_1EC32E: @ 81EC32E
	.string "I’m going to catch a whole bunch of\n"
	.string "POKéMON!$"

Route102_Text_1EC35B: @ 81EC35B
	.string "ROUTE 102\n"
	.string "{RIGHT_ARROW} OLDALE TOWN$"

Route102_Text_1EC373: @ 81EC373
	.string "ROUTE 102\n"
	.string "{LEFT_ARROW} PETALBURG CITY$"

