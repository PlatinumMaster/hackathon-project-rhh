const struct MonCoords gTrainerBackPicCoords[] =
{
    {8, 4},
    {8, 4},
    {8, 5},
    {8, 5},
    {8, 4},
    {8, 4},
    {8, 4},
    {8, 4},
};

const struct CompressedSpriteSheet gTrainerBackPicTable[] =
{
    gTrainerBackPic_AlexMale, 0x2000, 0,
    gTrainerBackPic_AlexFemale, 0x2000, 1,
    gTrainerBackPic_Red, 0x2800, 2,
    gTrainerBackPic_Leaf, 0x2800, 3,
    gTrainerBackPic_RubySapphireBrendan, 0x2000, 4,
    gTrainerBackPic_RubySapphireMay, 0x2000, 5,
    gTrainerBackPic_Wally, 0x2000, 6,
    gTrainerBackPic_Steven, 0x2000, 7,
};

const struct CompressedSpritePalette gTrainerBackPicPaletteTable[] = 
{
    gTrainerBackPicPalette_AlexMale, 0,
    gTrainerBackPicPalette_AlexFemale, 1,
    gTrainerBackPicPalette_Red, 2,
    gTrainerBackPicPalette_Leaf, 3,
    gTrainerPalette_RubySapphireBrendan, 4,
    gTrainerPalette_RubySapphireMay, 5,
    gTrainerPalette_Wally, 6,
    gTrainerPalette_Steven, 7,
};
